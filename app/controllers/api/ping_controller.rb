class Api::PingController < ApplicationController
  def show
    render json: "OK", status: :ok
  end
end
