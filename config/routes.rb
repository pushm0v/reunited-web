Rails.application.routes.draw do
  get 'ping', to: 'api/ping#show', defaults: { format: :json }
  root :to => 'web/index#index'
  namespace :web do
    resources :index, :only => [:index]
    resources :gallery, :only => [:index]
    resources :committee, :only => [:index]
  end
end
